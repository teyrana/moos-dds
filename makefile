BUILD_TYPE="debug"
SHELL:=bash

# local paths (host system)
CONTEXT_PATH=${PWD}

# container parameters (container / client system)
CONTAINER_CONTAINER_NAME=moos-ivp-extend-vscode-dev
CONTAINER_FILE_PATH=${CONTEXT_PATH}/.devcontainer/dockerfile.ubuntu
CONTAINER_IMAGE_TAG=moos-ivp-extend
CONTAINER_IMAGE_LABEL=devel
CONTAINER_PROJECT_PATH=/workspaces/moos-ivp-extend

#-------------------------------------------------------------------
#  Part 2: Invoke the call to make in the build directory
#-------------------------------------------------------------------

# default target goes first
.PHONY: default
default: example

# -------------------- Define Rest of Targets -------------------------------

attach: container-attach

.PHONY: build
build/src/ingest: build
build/src/trackmon: build
build: #build/CMakeCache.txt
	cd build && ninja

.PHONY: config, configure
config:configure
build/CMakeCache.txt: configure
configure:
	cd build && cmake -GNinja -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..

container-attach:
	docker exec -it -w ${CONTAINER_PROJECT_PATH} ${CONTAINER_CONTAINER_NAME} ${SHELL}

container-build: 
	docker build --tag=${CONTAINER_IMAGE_TAG}:${CONTAINER_IMAGE_LABEL} --file=${CONTAINER_FILE_PATH} ${CONTEXT_PATH}

container-exec: container-attach

run: container-run
container-run:CONTAINER_MOUNT_ARGS="type=bind,source=${CONTEXT_PATH},destination=${CONTAINER_PROJECT_PATH}"
container-run: container-build
	docker run -it --rm -w ${CONTAINER_PROJECT_PATH} --mount=${CONTAINER_MOUNT_ARGS} ${CONTAINER_IMAGE_TAG}:${CONTAINER_IMAGE_LABEL}


example: build
	bin/pExampleApp


clean: 
	rm -rf build/*

