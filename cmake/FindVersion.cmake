execute_process(COMMAND git rev-parse HEAD
                OUTPUT_VARIABLE GIT_REV_LONG
                ERROR_QUIET)

# Check whether we got any revision (will not if not in repo)
if ("${GIT_REV_LONG}" STREQUAL "")
    set(GIT_REV_LONG "N/A")
    set(GIT_REV_SHORT "N/A")
    set(GIT_DIFF "") # default to no changes
    set(GIT_TAG "N/A")
    set(GIT_BRANCH "N/A")
    set(GIT_DESCRIBE "N/A")
else()
    execute_process(
        COMMAND git log --pretty=format:'%h' -n 1
        OUTPUT_VARIABLE GIT_REV_SHORT
    )
    execute_process(
        COMMAND bash -c "git diff --quiet --exit-code || echo +"
        OUTPUT_VARIABLE GIT_DIFF
    )
    execute_process(
        COMMAND git describe --tags --abbrev=0
        OUTPUT_VARIABLE GIT_TAG ERROR_QUIET
    )
    execute_process(
        COMMAND git rev-parse --abbrev-ref HEAD
        OUTPUT_VARIABLE GIT_BRANCH
    )
    execute_process(
        COMMAND git describe --tags
        OUTPUT_VARIABLE GIT_DESCRIBE
    )

    set(CPU_ARCH ${CMAKE_SYSTEM_PROCESSOR})

    # strip newlines off all strings
    string(STRIP "${GIT_REV_LONG}" GIT_REV_LONG)
    string(STRIP "${GIT_REV_SHORT}" GIT_REV_SHORT)
    string(SUBSTRING "${GIT_REV_SHORT}" 1 7 GIT_REV_SHORT) # remove quotes
    string(STRIP "${GIT_DIFF}" GIT_DIFF)
    string(STRIP "${GIT_TAG}" GIT_TAG)
    string(STRIP "${GIT_BRANCH}" GIT_BRANCH)
    string(STRIP "${GIT_DESCRIBE}" GIT_DESCRIBE)
    string(STRIP "${CPU_ARCH}" CPU_ARCH )

endif()

# message(STATUS "::DEBUG::GIT_REV_LONG    = ${GIT_REV_LONG}")
# message(STATUS "::DEBUG::GIT_REV_SHORT   = ${GIT_REV_SHORT}")
# message(STATUS "::DEBUG::GIT_DIFF        = ${GIT_DIFF}")
# message(STATUS "::DEBUG::GIT_TAG         = ${GIT_TAG}")
# message(STATUS "::DEBUG::GIT_BRANCH      = ${GIT_BRANCH}")
# message(STATUS "::DEBUG::GIT_DESCRIBE    = ${GIT_DESCRIBE}")
# message(STATUS "::DEBUG::CPU_ARCH        = ${CPU_ARCH}")

MARK_AS_ADVANCED(GIT_REV_LONG, GIT_REV_SHORT, GIT_DIFF, GIT_TAG, GIT_BRANCH, GIT_DESCRIBE)

add_definitions(-DGIT_REV_LONG=${GIT_REV_LONG})
add_definitions(-DGIT_REV_SHORT=${GIT_REV_SHORT})
add_definitions(-DGIT_DIFF=${GIT_DIFF})
add_definitions(-DGIT_TAG=${GIT_TAG})
add_definitions(-DGIT_BRANCH=${GIT_BRANCH})
add_definitions(-DGIT_DESCRIBE=${GIT_DESCRIBE})
