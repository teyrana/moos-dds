# - Find libpcap
# Find the native PCAP includes and library
#
#  MOOS_FOUND             - True if moos found
#  MOOS_IVP_FOUND         - True if moos-ivp found
#  MOOS_IVP_BINARY_PATHS  - Where to find MOOSDB, etc
#  MOOS_IVP_INCLUDE_PATHS - all MOOS-IvP include paths
#  MOOS_IVP_LINK_PATHS    - paths to look for relevant libraries
#  MOOS_IVP_LIBRARIES     - available MOOS & MOOS-IvP libraries


set( MOOS_IVP_PATH $ENV{MOOS_IVP_PATH} )
message(STATUS "::Loading:ENV:MOOS_IVP_PATH: ${MOOS_IVP_PATH}")

if( NOT MOOS_IVP_PATH )
    find_path(MOOS_IVP_PATH
        NAMES
            bin/MOOSDB
        PATHS
            # path from the (CI) docker image
            /home/moos/moos-ivp
            # path from (Docker) devcontainers
            /workspaces/moos-ivp
        REQUIRED
    )
endif()

message(STATUS "::Using: MOOS_IVP_PATH: ${MOOS_IVP_PATH}")

find_path(MOOS_IVP_BINARY_PATH
    NAMES
        MOOSDB
    PATHS
        "${MOOS_IVP_PATH}/bin"
    REQUIRED
)

find_path(MOOS_INCLUDE_PATH
    NAMES
        MOOS/libMOOS/MOOSLib.h
    PATHS
        "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/include/"
    REQUIRED
)
find_path(IVP_INCLUDE_PATH
    NAMES
        MBUtils.h
    PATHS
        "${MOOS_IVP_PATH}/include/ivp"
    REQUIRED
)

find_path(MOOS_LINK_PATH
    NAMES
        libMOOS.a
    PATHS
        "${MOOS_IVP_PATH}/build/MOOS/MOOSCore/lib"
    REQUIRED
)
find_path(IVP_LINK_PATH
    NAMES
        libivpcore.a
    PATHS
        "${MOOS_IVP_PATH}/lib"
    REQUIRED
)

find_path( GEODESY_LINK_PATH
    NAMES
        libMOOSGeodesy.a
        libMOOSGeodesy.so
    PATHS
        "${MOOS_IVP_PATH}/build/MOOS/MOOSGeodesy/lib/"
    REQUIRED
)

find_path( PROJ_LINK_PATH
    NAMES
        libproj.a
        libproj.so
    PATHS
        "${MOOS_IVP_PATH}/build/MOOS/proj-5.2.0/lib"
        "${MOOS_IVP_PATH}/build/MOOS/proj-5.2.0/lib64"
    REQUIRED
)

if(MOOS_IVP_BINARY_PATH)
    set(MOOS_FOUND 1)
    set(MOOS_IVP_FOUND 1)

    set( MOOS_IVP_BINARY_PATHS ${MOOS_IVP_BINARY_PATH})

    set( MOOS_IVP_INCLUDE_PATHS ${MOOS_INCLUDE_PATH} ${IVP_INCLUDE_PATH})
    LIST(APPEND
        MOOS_IVP_INCLUDE_PATHS
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/App/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Comms/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/DB/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Utils/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Thirdparty/PocoBits/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Thirdparty/getpot/include"
            "${MOOS_IVP_PATH}/MOOS/MOOSCore/Core/libMOOS/Thirdparty/AppCasting/include"
    )
    
    set( MOOS_IVP_LINK_PATHS ${MOOS_LINK_PATH} 
                            ${IVP_LINK_PATH}
                            ${GEODESY_LINK_PATH}
                            ${PROJ_LINK_PATH})

    FILE(GLOB MOOS_LIBRARIES RELATIVE ${MOOS_LINK_PATH} ${MOOS_LINK_PATH}/lib* )
    FILE(GLOB IVP_LIBRARIES RELATIVE ${IVP_LINK_PATH} ${IVP_LINK_PATH}/lib* )
    FILE(GLOB GEODESY_LIBRARIES RELATIVE ${GEODESY_LINK_PATH} ${GEODESY_LINK_PATH}/lib* )
    FILE(GLOB PROJ_LIBRARIES RELATIVE ${PROJ_LINK_PATH} ${PROJ_LINK_PATH}/lib* )
    set( MOOS_IVP_LIBRARIES ${MOOS_LIBRARIES}
                            ${IVP_LIBRARIES}
                            ${GEODESY_LIBRARIES}
                            ${PROJ_LIBRARIES})

    mark_as_advanced( MOOS_FOUND )
    mark_as_advanced( MOOS_IVP_FOUND )
    mark_as_advanced( MOOS_IVP_BINARY_PATHS )
    mark_as_advanced( MOOS_IVP_INCLUDE_PATHS )
    mark_as_advanced( MOOS_IVP_LINK_PATHS )
    mark_as_advanced( MOOS_IVP_LIBRARIES )
endif()
